%define gtk3_version 3.24
%define glib_version 2.58
%define vte_version 0.57
%define libhandy_version 1.1.90
%define gettext_version 0.19.8
%define app_id org.gnome.zbrown.KingsCross
%define realname kgx

Name: kgx-development
Version: master
Release: 0%{?dist}
Summary: A minimal terminal for GNOME

License: GPLv3+
URL: https://gitlab.gnome.org/ZanderBrown/kgx
Source0: https://gitlab.gnome.org/ZanderBrown/%{realname}/-/archive/%{version}/%{realname}-%{version}.tar.gz

BuildRequires: pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires: pkgconfig(gio-2.0) >= %{glib_version}
BuildRequires: pkgconfig(libhandy-1) >= %{libhandy_version}
BuildRequires: pkgconfig(vte-2.91) >= %{vte_version}
BuildRequires: pkgconfig(libgtop-2.0)
BuildRequires: desktop-file-utils
BuildRequires: gettext >= %{gettext_version}
BuildRequires: clang
BuildRequires: meson
BuildRequires: ninja-build
BuildRequires: sassc
BuildRequires: nautilus-devel

Requires: gtk3%{?_isa} >= %{gtk3_version}
Requires: libhandy1%{?_isa} >= %{libhandy_version}
Requires: vte291%{?_isa} >= %{vte_version}
Requires: libgtop2%{?_isa}

%description
KGX is supposed to be a simple terminal emulator for the average user to carry
out simple cli tasks and aims to be a 'core' app for GNOME/Phosh.

%package nautilus
Summary: KGX extension for Nautilus
Requires: %{name}%{?_isa} = %{version}-%{release}

%description nautilus
This package provides a Nautilus extension that adds the 'Open in Terminal'
option to the right-click context menu in Nautilus.

%prep
%setup -q -n %{realname}-%{version}

%build
%meson -Dgtop=true
%meson_build

%install
%meson_install
%find_lang %{realname} --with-gnome

%files -f %{realname}.lang
%license COPYING
%doc README.md
%{_bindir}/%{realname}
%{_datadir}/dbus-1/services/%{app_id}.service
%{_datadir}/glib-2.0/schemas/%{app_id}.gschema.xml
%{_datadir}/applications/%{app_id}.desktop
%{_datadir}/icons/hicolor/*/apps/%{app_id}.svg
%{_datadir}/icons/hicolor/symbolic/apps/%{app_id}-symbolic.svg
%{_datadir}/icons/hicolor/*/apps/%{app_id}.Generic.svg
%{_datadir}/icons/hicolor/symbolic/apps/%{app_id}.Generic-symbolic.svg
%{_datadir}/metainfo/%{app_id}.appdata.xml
%{_libdir}/kgx/libkgx.so

%files nautilus
%{_libdir}/nautilus/extensions-3.0/libkgx-nautilus.so

%changelog
* Thu Oct 24 2019 Christopher Davis <christopherdavis@gnome.org> - master-0
- Initial RPM
